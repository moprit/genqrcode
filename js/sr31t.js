var currentKey = '';
jQuery(document).ready(function ($) {
	$('body').keypress(function (e) {
		var code = e.keyCode || e.which;
		if (code == 13) {
			$('body').append('<div>Code: ' + currentKey + '</div>');

			currentKey = '';
		} else {
			currentKey += String.fromCharCode(code);
		}
		e.preventDefault();
	});
});

var getCurrentVisitor = function () {
	setTimeout(function () {
		jQuery.get(GenQrCode.baseUrl + '/index.php/current/' + currentLine, function (data) {
			if (data.result) {

				$('body').addClass('visited');
				$('.visitor').show();

				if (data.visitor.name === 'Quý khách') {
					$('.visitor .photo').html("");
					$('.visitor .welcome').html("");
					$('.visitor .name').html("");
					$('.visitor .job_title').html("");
					$('.visitor .company').html("");
					$('.visitor .client').html("Chào mừng Anh Chị");
				} else {
					if (data.visitor.photo !== '') {
						$('.visitor').addClass('photo-screen');
						$('.visitor .photo').html('<img alt="" src="' + data.visitor.photo + '"/>');
					} else {
						$('.visitor').removeClass('photo-screen');
						$('.visitor .photo').html("");
					}

					$('.visitor .welcome').html("Chào mừng " + data.visitor.subtitle);
					$('.visitor .name').html(data.visitor.name);
					$('.visitor .job_title').html(data.visitor.job_title);
					$('.visitor .company').html(data.visitor.company);
					$('.visitor .client').html("");
				}
			} else {
				$('body').removeClass('visited');

				$('.visitor').hide();
			}
		}, 'JSON');

		getCurrentVisitor();
	}, 3000);
};