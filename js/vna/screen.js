var SEATS = [];

jQuery(document).ready(function ($) {
	autoSync(true);
});

var autoSync = function (firstTime) {
	setTimeout(function () {
		jQuery.get(GenQrCode.baseUrl + '/index.php/vna/status/', {limit : (firstTime ? 500 : 20)}, function (res) {
			if (res.result) {
				if (res.data.length) {
					for (var i = 0, n = res.data.length; i < n; i++) {
						var seat = res.data[i];
						if (seat.visited) {
							$('.seat_' + seat.seat_number).css({'backgroundColor' : seat.seat_color});

							if (!firstTime && SEATS[seat.seat_number] === undefined) {
								addQueue(seat);
							}

							SEATS[seat.seat_number] = 1;
						}
					}
				}
			}
		}, 'JSON');

		autoSync(false);
	}, 2000);
};

var addQueue = function (seat) {

	$('.visitorQueue').append(genQueueItem(seat));
	$('.visitorQueue').removeClass('hidden');

	$('#itemQueue_' + seat.seat_number).css({top : 0, left : '-300px'});
	$('.seat_' + seat.seat_number).notify( $('.seat_' + seat.seat_number).attr('rel') + ": " + seat.seat_number, {
		autoHideDelay : 10000,
		className : 'info',
		position : $('.seat_' + seat.seat_number).attr('rel')
	});

	$('.itemQueue').each(function (index, item) {
		$(item).animate({
			left : "+=300"
		}, 1000, function () {
			var me = $(this);
			if (me.css('left') >= $(window).width()) {
				me.remove();

				if (!$('.itemQueue').length) {
					$('.visitorQueue').addClass('hidden');
				}
			} else {
				setTimeout(function () {
					me.animate({
						left : $(window).width()
					}, 5000, function () {
						me.remove();

						if (!$('.itemQueue').length) {
							$('.visitorQueue').addClass('hidden');
						}
					});
				}, 1000 * 60 * 5);
			}
		});
	});


};

var genQueueItem = function (seat) {

	var innerHTML = '';
	innerHTML += '<div class="span3 itemQueue" id="itemQueue_' + seat.seat_number + '">';
	innerHTML += '<div href="#" class="thumbnail itemContent food_set_' + seat.food_set + '">';
	innerHTML += '<div class="name">' + seat.name + '</div>';
	innerHTML += '<div class="seat_number">' + seat.seat_number + '</div>';
	innerHTML += '<div class="table_number">' + seat.table_number + '</div>';
	innerHTML += '</div>';
	innerHTML += '</div>';

	return innerHTML;
}