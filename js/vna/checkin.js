var currentKey = '';
var visitorStore = [];

jQuery(document).ready(function ($) {
	// autoCheckin();
	// autoSync(true);

	checkin();
});

var checkin = function () {
	$('body').keypress(function (e) {
		var code = e.keyCode || e.which;
		if (code == 13) {
			updateCurrentVisitor(currentKey);
			currentKey = '';
		} else {
			currentKey += String.fromCharCode(code);
		}
		e.preventDefault();
	});
};

var updateCurrentVisitor = function (currentKey) {
	jQuery.get(GenQrCode.baseUrl + '/index.php/vna/visit/' + currentKey, function (res) {

		var result = '';
		if (res.result) {
			result = '[' + res.data.membership_number + '] ' + res.data.name;
		} else {
			result = 'Error: ' + res.message;
		}
		$('.attendee').css({'color' : '#000000'});
		$('body').prepend('<div class="attendee" style="font-size: 30px; margin-bottom: 20px; color: #FF0000">' + result + '</div>');
		// $('body').html('<div>' + result + '</div>');
	}, 'JSON');
};

var autoCheckin = function () {
	setTimeout(function () {
		jQuery.get(GenQrCode.baseUrl + '/index.php/vna/genMock/', function (res) {
			if (res.result) {


			}
		}, 'JSON');

		autoCheckin();
	}, 2000);
};

var autoSync = function (getAll) {
	setTimeout(function () {
		jQuery.get(GenQrCode.baseUrl + '/index.php/vna/status/', {limit : (getAll ? 420 : 20)}, function (res) {
			if (res.result) {
				if (res.data.length) {
					for (var i = 0, n = res.data.length; i < n; i++) {
						var seat = res.data[i];
						$('.seat_' + seat.seat_number).css({'backgroundColor' : seat.seat_color});
					}
				}
			}
		}, 'JSON');

		autoSync(false);
	}, 2000);
};