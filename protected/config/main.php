<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'   => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name'       => 'QR-Code Generator',
	// preloading 'log' component
	'preload'    => array(
		'log',
		'bootstrap'
	),
	// autoloading model and component classes
	'import'     => array(
		'application.models.*',
		'application.models.base.*',
		'application.components.*'
	),
	'modules'    => array( // uncomment the following to enable the Gii tool
		'gii' => array(
			'class'     => 'system.gii.GiiModule',
			'password'  => '123456',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters' => array('127.0.0.1', '::1'),
		),
		'aia',
		'vna'
	),
	// application components
	'components' => array(
		'user'         => array(
			// enable cookie-based authentication
			'allowAutoLogin' => TRUE,
		),
		'urlManager'   => array(
			'urlFormat'      => 'path',
			'showScriptName' => TRUE,
			'caseSensitive'  => FALSE,
			'rules'          => array(
				// VNA Event
				'vna/visit/<key:\w+>'                    => 'vna/default/visit',
				'vna/<action:\w+>'                       => 'vna/default/<action>',
				// AIA Event
				'aia/visit/<key:\w+>'                    => 'aia/default/visit',
				'aia/visit/<key:\w+>/<line:\d+>'         => 'aia/default/visit',
				'current/'                               => 'site/current',
				'current/<line:\d+>'                     => 'site/current',
				'screen/<line:\d+>'                      => 'site/screen',
				'<controller:\w+>/<id:\d+>'              => '<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>'          => '<controller>/<action>',
			),
		),
		'db'           => array(
			'connectionString' => 'mysql:host=localhost;dbname=genQrCode',
			'emulatePrepare'   => TRUE,
			'username'         => 'root',
			'password'         => '123456',
			'charset'          => 'utf8',
		),
		'errorHandler' => array(
			// use 'site/error' action to display errors
			'errorAction' => 'site/error',
		),
		'log'          => array(
			'class'  => 'CLogRouter',
			'routes' => array(
				array(
					'class'  => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
				array(
					'class'  => 'CWebLogRoute',
					'levels' => 'error, warning',
				),
			),
		),
		'yexcel'       => array(
			'class' => 'ext.yexcel.Yexcel'
		),
		'bootstrap'    => array(
			'class' => 'application.components.Bootstrap',
		),
	),
	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'     => array(
		// this is used in contact page
		'adminEmail' => 'webmaster@example.com',
	),
);