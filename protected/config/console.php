<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'   => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name'       => 'QR-Code Generator',
	// preloading 'log' component
	'preload'    => array('log'),
	// autoloading model and component classes
	'import'     => array(
		'application.models.*',
		'application.models.base.*',
		'application.components.*',
		'application.modules.*',
		'application.modules.vna.*',
		'application.modules.vna.models.*',
		'application.modules.vna.models.base.*',
	),
	'modules'    => array( // uncomment the following to enable the Gii tool
		//'aia',
		'vna'
	),
	// application components
	'components' => array(
		'db'     => array(
			'connectionString' => 'mysql:host=localhost;dbname=genQrCode',
			'emulatePrepare'   => TRUE,
			'username'         => 'root',
			'password'         => '123456',
			'charset'          => 'utf8',
		),
		'log'    => array(
			'class'  => 'CLogRouter',
			'routes' => array(
				array(
					'class'  => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
			),
		),
		'yexcel' => array(
			'class' => 'ext.yexcel.Yexcel'
		)
	),
);