<?php
/* @var DefaultController $this */
/* @var Array $seats */

Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . "/js/bootstrap.min.js", CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . "/js/notify.min.js", CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . "/js/vna/screen.js", CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . "/css/vna.css");

$pos = array( // top, left
	'1'  => array(231, 492),
	'2'  => array(231, 599),
	'11' => array(381, 492),
	'12' => array(381, 599),
	'3'  => array(231, 385),
	'4'  => array(231, 705),
	'13' => array(381, 385),
	'14' => array(381, 705),
	'5'  => array(153, 278),
	'6'  => array(153, 811),
	'15' => array(362, 278),
	'16' => array(362, 811),
	'7'  => array(153, 172),
	'8'  => array(153, 917),
	'17' => array(362, 172),
	'18' => array(362, 917),
	'9'  => array(153, 65),
	'10' => array(153, 1024),
	'19' => array(362, 65),
	'20' => array(362, 1024)
);

$css = "";
foreach (VnaVisitor::$TABLE_ORDER as $table) {
	if (!isset($pos[$table])) {
		continue;
	}

	$index = 0;
	$css .= sprintf("/* -- TABLE %s -- */ \n", $table);
	for ($i = VnaVisitor::$TABLE_RANGE[$table][0], $n = VnaVisitor::$TABLE_RANGE[$table][1]; $i <= $n; $i += 4) {
		$css .= sprintf(".seat_%s {top: %spx; left: %spx} \n", $i, ($pos[$table][0] + (18 * $index)), $pos[$table][1]);
		$css .= sprintf(".seat_%s {top: %spx; left: %spx} \n", ($i + 2), ($pos[$table][0] + (18 * $index)), $pos[$table][1] + 64);

		$index++;
	}
}
Yii::app()->clientScript->registerCss("room-wrapper", $css);

$this->widget('bootstrap.widgets.TbNavbar', array(
	'brand'    => '<img src="' . Yii::app()->request->baseUrl . '/images/vna/logo.png" class="logo" /> Sơ đồ bố trí bàn tiệc',
	'brandUrl' => '#',
	'fixed'    => 'top',
	'items'    => array(
		array(
			'htmlOptions' => array('class' => 'pull-right'),
			'class'       => 'bootstrap.widgets.TbMenu',
			'items'       => array(
				array('label' => 'Đón khách', 'url' => Yii::app()->createUrl('/vna/')),
				array('label' => 'Sơ đồ bàn tiệc', 'url' => '#', 'active' => TRUE),
				array('label' => 'Báo cáo', 'url' => Yii::app()->createUrl('/vna/report/'))
			)
		)
	)
));

?>

<div class="roomWrapper container">
	<?php $posNotify = 0; ?>
	<?php foreach ($seats as $seat) : ?>
		<div class="seat <?php echo $seat['class_name']; ?>" rel="<?php echo($posNotify == 0 ? 'left' : 'right'); ?>">
			<span><?php echo $seat['id']; ?></span>
		</div>
		<?php $posNotify = 1 - $posNotify; ?>
	<?php endforeach; ?>
</div>

<div class="visitorQueue navbar navbar-fixed-bottom hidden">

</div>