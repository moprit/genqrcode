<?php
/* @var DefaultController $this */

Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . "/css/vna.css");

$this->widget('bootstrap.widgets.TbNavbar', array(
	'brand'    => '<img src="' . Yii::app()->request->baseUrl . '/images/vna/logo.png" class="logo" /> Vietnam Airlines xin kính chào quý khách',
	'brandUrl' => '#',
	'fixed'    => 'top',
	'items'    => array(
		array(
			'htmlOptions' => array('class' => 'pull-right'),
			'class'       => 'bootstrap.widgets.TbMenu',
			'items'       => array(
				array('label' => 'Đón khách', 'url' => '#', 'active' => TRUE),
				array('label' => 'Sơ đồ bàn tiệc', 'url' => Yii::app()->createUrl('/vna/screen/')),
				array('label' => 'Báo cáo', 'url' => Yii::app()->createUrl('/vna/report/'))
			)
		)
	)
));

?>