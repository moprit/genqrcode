<?php

class DefaultController extends Controller {

	var $SEAT_COLOR = array(
		1 => '#FF0000',
		2 => '#FFFF00',
		3 => '#0000FF'
	);

	const MAX_SEAT = 416;


	public function actionIndex() {
		$this->layout = "//layouts/vna";
		$this->render('index');
	}

	public function actionScreen() {
		$this->layout = "//layouts/vna";

		$seats = array();
		for ($i = 1, $n = DefaultController::MAX_SEAT; $i <= $n; $i++) {
			$seats[] = array(
				'id'         => $i,
				'class_name' => sprintf('seat_%s', $i),
				'color'      => ''
			);
		}

		$this->render('screen', array('seats' => $seats));
	}

	/**
	 * @param string $key
	 */
	public function actionVisit($key = '') {
		header('Vary: Accept');
		header('Content-type: application/json');

		/* @var VnaVisitor $model */
		$model = VnaVisitor::model()->findByAttributes(array('key' => $key));
		if ($model === NULL) {
			echo CJSON::encode(array('result' => FALSE, 'message' => 'Visitor cannot be found (' . $key . ")"));
			Yii::app()->end();
		}

		$model->checkIn();

		echo CJSON::encode(array(
			'result' => TRUE,
			'data'   => $model
		));
	}


	/**
	 * Get the status of all attendees.
	 *
	 */
	public function actionStatus() {
		header('Vary: Accept');
		header('Content-type: application/json');

		$limit = Yii::app()->request->getParam('limit', 10);
		//$limit = 500;
		/**
		 * @var VnaVisitor[] $models
		 */
		$models = VnaVisitor::model()->findAll(array("order" => 'date_visited DESC', 'limit' => $limit));
		if ($models === NULL) {
			echo CJSON::encode(array('result' => FALSE, 'message' => 'Database is empty.'));
			Yii::app()->end();
		}

		$result = array();
		if (count($models)) {
			foreach ($models as $model) {
				$result[] = array(
					'name'         => $model->name,
					'seat_number'  => (empty($model->seat_number) ? 0 : $model->seat_number),
					'table_number' => (empty($model->table_number) ? 0 : $model->table_number),
					'food_set'     => $model->food_set,
					'seat_color'   => isset($this->SEAT_COLOR[$model->food_set]) ? $this->SEAT_COLOR[$model->food_set] : '#000000',
					'visited'      => (!empty($model->date_visited))
				);
			}
		}

		echo CJSON::encode(array(
			'result' => TRUE,
			'data'   => $result
		));
	}


	/**
	 * Get the status of all attendees.
	 *
	 */
	public function actionSync() {
		header('Vary: Accept');
		header('Content-type: application/json');

		/**
		 * @var VnaVisitor[] $models
		 */
		$models = VnaVisitor::model()->findAll(array("order" => 'date_visited DESC'));
		if ($models === NULL) {
			echo CJSON::encode(array('result' => FALSE, 'message' => 'Database is empty.'));
			Yii::app()->end();
		}

		$result = array();
		if (count($models)) {
			foreach ($models as $model) {
				$result[] = array(
					'key'               => $model->key,
					'name'              => $model->name,
					'membership_number' => $model->membership_number,
					'company'           => $model->company,
					'job_title'         => $model->job_title,
					'foot_set'          => $model->food_set,
					'seat_color'        => isset($this->SEAT_COLOR[$model->food_set]) ? $this->SEAT_COLOR[$model->food_set] : '',
					'seat_number'       => $model->seat_number,
					'table_number'      => $model->table_number,
					'visited'           => (!empty($model->date_visited)),
					'reserved'          => $model->reserved,
					'date_visited'      => $model->date_visited
				);
			}
		}

		echo CJSON::encode(array(
			'result' => TRUE,
			'data'   => $result
		));
	}

	/**
	 *
	 */
	public function actionReset() {
		header('Vary: Accept');
		header('Content-type: application/json');

		VnaVisitor::model()->updateAll(array('date_visited' => NULL, 'last_updated' => NULL));
		VnaVisitor::model()->updateAll(array('seat_number' => 0, 'table_number' => 0), "reserved <> 1");

		echo CJSON::encode(array(
			'result'  => TRUE,
			'message' => 'Data has just been updated successfully.'
		));
	}

	/**
	 *
	 */
	public function actionGenMock() {
		header('Vary: Accept');
		header('Content-type: application/json');

		$limit = 1; //rand(1, 50);
		/**
		 * @var VnaVisitor[] $models
		 */
		$models = VnaVisitor::model()->findAllByAttributes(array(
			"date_visited" => NULL
		), array(
			"order" => 'RAND()',
			'limit' => $limit
		));
		if ($models === NULL) {
			echo CJSON::encode(array('result' => FALSE, 'message' => 'Database is empty.'));
			Yii::app()->end();
		}

		if (count($models)) {
			foreach ($models as $model) {
				$model->checkIn();
			}
		}

		echo CJSON::encode(array(
			'result'  => TRUE,
			'message' => 'We have new ' . $limit . ' attendees',
			'data'    => $models
		));
	}

	/**
	 *
	 */
	public function actionFoodSet() {

		$file_path = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'visitor_vna_foodset.xls';
		if (!file_exists($file_path)) {
			throw new Exception("File not found: " . $file_path);
			Yii::app()->end();
		}


		$sheet_array = Yii::app()->yexcel->readActiveSheet($file_path);

		echo '<table>';
		foreach ($sheet_array as $row) {
			$model = VnaVisitor::model()->findByAttributes(array('membership_number' => $row['A']));
			if ($model !== NULL && !empty($row['B'])) {
				echo '<tr>';
				echo '<td>' . $row['A'] . '</td>';
				echo '<td>' . $row['B'] . '<br />' . $row['C'] . '</td>';
				echo '<td>' . $model->name . '<br/>' . $model->company . '</td>';
				echo '<td>' . $row['E'] . '</td>';
				echo '<td>' . (ucwords(strtolower($row['B'])) == $model->name ? '' : 'ERROR') . '</td>';
				echo '</tr>';

				$model->company   = $row['C'];
				$model->job_title = $row['D'];
				$model->food_set  = $row['E'];
				$model->save();
			} else {
				echo '<tr>';
				echo '<td>' . $row['A'] . '</td>';
				echo '<td>' . $row['B'] . '<br />' . $row['C'] . '</td>';
				echo '<td>' . '</td>';
				echo '<td>' . $row['E'] . '</td>';
				echo '<td>' . 'NOT EXISTED' . '</td>';
				echo '</tr>';
			}
		}
		echo '</table>';
	}

	/**
	 *
	 */
	public function actionReserved() {


		$file_path = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'visitor_vna_reserved.xls';
		if (!file_exists($file_path)) {
			throw new Exception("File not found: " . $file_path);
			Yii::app()->end();
		}

		$sheet_array = Yii::app()->yexcel->readActiveSheet($file_path);

		echo '<table>';
		foreach ($sheet_array as $row) {
			/** @var VnaVisitor $model */
			$model = VnaVisitor::model()->findByAttributes(array('membership_number' => $row['A']));
			if ($model !== NULL && !empty($row['B'])) {
				echo '<tr>';
				echo '<td>' . $row['A'] . '</td>';
				echo '<td>' . $row['B'] . '<br />' . $row['C'] . '</td>';
				echo '<td>' . $model->name . '<br/>' . $model->company . '</td>';
				echo '<td>Seat: ' . $row['D'] . '</td>';
				echo '<td>Table: ' . $row['E'] . '</td>';
				echo '<td>' . (ucwords(strtolower($row['B'])) == $model->name ? '' : 'ERROR') . '</td>';
				echo '</tr>';

				$model->seat_number  = $row['D'];
				$model->table_number = $row['E'];
				$model->reserved     = 1;
				$model->save();
			} else {
				echo '<tr>';
				echo '<td>' . $row['A'] . '</td>';
				echo '<td>' . $row['B'] . '<br />' . $row['C'] . '</td>';
				echo '<td></td>';
				echo '<td>Seat: ' . $row['D'] . '</td>';
				echo '<td>Table: ' . $row['E'] . '</td>';
				echo '<td>' . 'NOT EXISTED' . '</td>';
				echo '</tr>';

			}
		}
		echo '</table>';
	}
}