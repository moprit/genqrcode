<?php

/**
 * This is the model class for table "visitor_vna".
 *
 * The followings are the available columns in table 'visitor_vna':
 * @property string $id
 * @property string $key
 * @property string $name
 * @property string $membership_number
 * @property string $company
 * @property string $job_title
 * @property integer $food_set
 * @property integer $seat_number
 * @property integer $table_number
 * @property integer $reserved
 * @property string $date_visited
 * @property integer $last_updated
 */
class VnaVisitorBase extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'visitor_vna';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('key, name, membership_number', 'required'),
			array('food_set, seat_number, table_number, reserved, last_updated', 'numerical', 'integerOnly'=>true),
			array('key', 'length', 'max'=>50),
			array('name, company, job_title', 'length', 'max'=>128),
			array('membership_number', 'length', 'max'=>20),
			array('date_visited', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, key, name, membership_number, company, job_title, food_set, seat_number, table_number, reserved, date_visited, last_updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'key' => 'Key',
			'name' => 'Name',
			'membership_number' => 'Membership Number',
			'company' => 'Company',
			'job_title' => 'Job Title',
			'food_set' => 'Food Set',
			'seat_number' => 'Seat Number',
			'table_number' => 'Table Number',
			'reserved' => 'Reserved',
			'date_visited' => 'Date Visited',
			'last_updated' => 'Last Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('key',$this->key,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('membership_number',$this->membership_number,true);
		$criteria->compare('company',$this->company,true);
		$criteria->compare('job_title',$this->job_title,true);
		$criteria->compare('food_set',$this->food_set);
		$criteria->compare('seat_number',$this->seat_number);
		$criteria->compare('table_number',$this->table_number);
		$criteria->compare('reserved',$this->reserved);
		$criteria->compare('date_visited',$this->date_visited,true);
		$criteria->compare('last_updated',$this->last_updated);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VnaVisitorBase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
