<?php

Yii::import('application.modules.vna.models.base.*');

/**
 * This is the model class for table "visitor".
 *
 * The followings are the available columns in table 'visitor':
 *
 * @property string  $id
 * @property string  $key
 * @property string  $name
 * @property string  $membership_no
 * @property string  $company
 * @property integer $food_set
 */
class VnaVisitor extends VnaVisitorBase {

	public static $TABLE_ORDER = array(1, 2, 11, 12, 3, 4, 13, 14, 5, 6, 15, 16, 7, 8, 17, 18, 9, 10, 19, 20);

	public static $TABLE_RANGE = array(
		'1'  => array(1, 27),
		'2'  => array(2, 28),
		'11' => array(29, 67),
		'12' => array(30, 68),
		'3'  => array(69, 95),
		'4'  => array(70, 96),
		'13' => array(97, 135),
		'14' => array(98, 136),
		'5'  => array(137, 175),
		'6'  => array(138, 176),
		'15' => array(177, 227),
		'16' => array(178, 228),
		'7'  => array(229, 267),
		'8'  => array(230, 268),
		'17' => array(269, 319),
		'18' => array(270, 320),
		'9'  => array(321, 359),
		'10' => array(322, 360),
		'19' => array(361, 415),
		'20' => array(362, 416)
	);

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 *
	 * @param string $className active record class name.
	 *
	 * @return VisitorBase the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}


	/**
	 * Scope
	 *
	 * @param int $limit
	 *
	 * @return Visitor
	 */
	public function lastVisit($limit = 1) {
		$this->getDbCriteria()->mergeWith(array(
			'order' => 'date_visited DESC',
			'limit' => $limit,
		));

		return $this;
	}

	/**
	 *
	 */
	public function genQrCode() {
		if (!$this->id) {
			return FALSE;
		}
		$basePath = Yii::app()->basePath;
		if (strpos($basePath, 'protected') !== FALSE) {
			$basePath .= DIRECTORY_SEPARATOR . '..';
		}
		$basePath .= DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'qrcode_vna';
		$filename = $basePath . DIRECTORY_SEPARATOR . sprintf('%s.png', $this->membership_number);

		return QrCodeHelper::gen($this->key, $filename, 500, 500);
	}

	/**
	 *
	 */
	public function checkIn() {
		if (empty($this->date_visited)) {
			list($seat, $table, $isVip) = $this->_grabASeat();
			$this->seat_number  = $seat;
			$this->table_number = $table;
			$this->date_visited = new CDbExpression("NOW()");
			$this->last_updated = time();

			### DEMO
			$this->food_set = (empty($this->food_set) ? rand(1, 3) : $this->food_set);
			##

			if (!$this->save()) {
				CVarDumper::dump($this->getErrors(), 10, 1);
			}
		}
	}

	/**
	 *
	 */
	private function _grabASeat() {

		$reserved = VnaVisitor::getReservedSeats();
		if (array_key_exists($this->id, $reserved)) {
			return array($this->seat_number, $this->table_number, TRUE);
		}

		$last = VnaVisitor::model()->find("seat_number <> 0 AND reserved <> 1  ORDER BY date_visited DESC, last_updated DESC");
		if ($last !== NULL) {
			$table = $last->table_number;
			$seat  = $last->seat_number;
		} else {
			$table = 1;
			$seat  = -1;
		}

		do {
			$seat += 2;
			$max = VnaVisitor::$TABLE_RANGE[$table][1];

			// Move to new table
			if ($seat > $max) {
				$table = $this->_grabATable($table);
				if (!$table) {
					$seat  = 0;
					$table = 0;
					break;
				}
				$seat = VnaVisitor::$TABLE_RANGE[$table][0];
			}
		} while (array_search($seat, $reserved) != FALSE);

		return array($seat, $table, FALSE);
	}

	/**
	 *
	 */
	public static function getReservedSeats() {
		$result   = array(
			'MC1' => 1,
			'MC2' => 71
		);
		$reserved = VnaVisitor::model()->findAllByAttributes(array("reserved" => 1));
		if (count($reserved)) {
			foreach ($reserved as $re) {
				$result[$re->id] = $re->seat_number;
			}
		}

		return $result;
	}

	/**
	 * @param $last
	 *
	 * @return int
	 */
	private function _grabATable($last) {
		$index = array_search($last, VnaVisitor::$TABLE_ORDER);

		if (!isset(VnaVisitor::$TABLE_ORDER[$index + 1])) {
			return 0;
		}

		return ($index === FALSE ? 1 : VnaVisitor::$TABLE_ORDER[$index + 1]);
	}

}
