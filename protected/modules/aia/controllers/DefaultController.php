<?php

class DefaultController extends Controller {

	public function actionIndex() {
		$this->render('index');
	}

	/**
	 *
	 * @param int $line
	 */
	public function actionScreen($line = 1) {
		$this->render('screen', array('line' => $line));
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionCurrent($line = 1) {
		header('Vary: Accept');
		header('Content-type: application/json');

		$period = 10;
		$model  = Visitor::model()->findBySql('SELECT * FROM visitor WHERE line_visited = ' . $line . ' AND UNIX_TIMESTAMP(date_visited) > UNIX_TIMESTAMP(NOW()) - ' . (60 * $period) . ' ORDER BY  date_visited DESC');
		if ($model !== NULL) {
			echo CJSON::encode(array(
				'result'  => TRUE,
				'visitor' => array(
					'subtitle'  => CHtml::encode($model->subtitle),
					'name'      => CHtml::encode($model->name),
					'company'   => CHtml::encode($model->company),
					'job_title' => CHtml::encode($model->job_title),
					'photo'     => (file_exists('images' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . $model->id . '.jpg') ? Yii::app()->baseUrl . '/images/photo/' . $model->id . '.jpg' : '')
				)
			));
		} else {
			echo CJSON::encode(array('result' => FALSE));
		}
	}


	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest) {
				echo $error['message'];
			} else {
				$this->render('error', $error);
			}
		}
	}

	/**
	 * @param string $key
	 * @param int    $line
	 */
	public function actionVisit($key = '', $line = 1) {
		header('Vary: Accept');
		header('Content-type: application/json');

		$model = Visitor::model()->findByAttributes(array('key' => $key));
		if ($model === NULL) {
			echo CJSON::encode(array('result' => FALSE, 'message' => 'Visitor cannot be found.'));
			Yii::app()->end();
		}

		$model->line_visited = $line;
		$model->date_visited = new CDbExpression("NOW()");
		$model->save();

		echo CJSON::encode(array(
			'result' => TRUE,
			'data'   => $model
		));
	}

	public function actionInitPhoto() {

		$fileListOfDirectory     = array();
		$pathTofileListDirectory = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'photo';

		if (!is_dir($pathTofileListDirectory)) {
			die(" Invalid Directory");
		}

		if (!is_readable($pathTofileListDirectory)) {
			die("You don't have permission to read Directory");
		}

		foreach (new DirectoryIterator ($pathTofileListDirectory) as $file) {
			if ($file->getBasename() !== '..' && $file->getBasename() !== '.') {
				list ($id, $name) = explode('-', $file->getBasename(), 2);
				rename($pathTofileListDirectory . DIRECTORY_SEPARATOR . $file->getBasename(), $pathTofileListDirectory . DIRECTORY_SEPARATOR . trim($id) . '.jpg');
				array_push($fileListOfDirectory, "[" . trim($id) . "]" . $file->getBasename());
			}
		}
		echo 'done';
	}

	/**
	 *
	 */
	public function actionReadExcel() {

		$file_path   = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'visitors.xls';
		$file_path   = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'visitors-2.xls';
		$file_path   = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'visitors-3.xls';
		$file_path   = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'visitors-4.xls';
		$file_path   = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'visitors-5.xls';
		$file_path   = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'visitors-6.xls';
		$sheet_array = Yii::app()->yexcel->readActiveSheet($file_path);

		echo "<table>";

		foreach ($sheet_array as $row) {
			echo "<tr>";
			$model = Visitor::model()->findByPk($row['A']);
			if ($model !== NULL && !empty($row['B'])) {
				echo "<td>OLD: " . $model->name . "</td>";
				$model->name = $row['B'];
				if (!empty($row['C'])) {
					$model->company = $row['C'];
				}
				if (!empty($row['D'])) {
					$model->job_title = $row['D'];
				}
				$model->save();
			}

			foreach ($row as $column) {
				echo "<td>$column</td>";
			}
			echo "<td>[" . $model->name . "]</td>";
			echo "</tr>";
		}

		echo "</table>";

		//or

		//echo first cell of excel file
		//echo $sheet_array[1]['A'];
	}
}