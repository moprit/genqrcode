<?php

class Visitor extends VisitorBase {

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     *
     * @return Visitor the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Scope
     * @param int $limit
     *
     * @return Visitor
     */
    public function lastVisit($limit = 1) {
        $this->getDbCriteria()->mergeWith(array(
            'order' => 'date_visited DESC',
            'limit' => $limit,
        ));

        return $this;
    }

    /**
     *
     */
    public function genQrCode() {
        if (!$this->id) {
            return FALSE;
        }
        $basePath = Yii::app()->basePath;
        if (strpos($basePath, 'protected') !== FALSE) {
            $basePath .= DIRECTORY_SEPARATOR . '..';
        }
        $basePath .= DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'qrcode';
        $filename = $basePath . DIRECTORY_SEPARATOR . sprintf('%s.png', $this->id);

        return QrCodeHelper::gen($this->key, $filename, 500, 500);
    }

    /**
     * @return bool|void
     */
    protected function beforeSave() {
        if (empty($this->key)) {
            $this->key = md5(uniqid() . ' - ' . $this->id);
        }

        return parent::beforeSave();
    }
}