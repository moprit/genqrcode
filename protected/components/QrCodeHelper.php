<?php

/**
 * Class QrCodeHelper
 */
class QrCodeHelper {

    /**
     * @param string $data
     * @param string $filename
     * @param int    $width
     * @param int    $height
     * @param int    $border
     * @param string $error
     * @param bool   $https
     * @param bool   $loadBalance
     */
    public static function gen($data, $filename, $width = 100, $height = 100, $border = 1, $error = "L", $https = FALSE, $loadBalance = FALSE) {

        if (!file_exists($filename)) {
            // build Google Charts URL:
            // secure connection ?
            $protocol = $https ? "https" : "http";

            // load balancing
            $host = "chart.googleapis.com";
            if ($loadBalance) {
                $parameters = "chs={$width}x{$height}&cht=qr&chl=$data";
                $host       = abs(crc32($parameters) % 10) . ".chart.apis.google.com";
            }

            // put everything together
            $qrUrl = "$protocol://$host/chart?chs={$width}x{$height}&cht=qr&chld=$error|$border&chl=$data";

            // get QR code from Google's servers
            $qr = file_get_contents($qrUrl);

            // optimize PNG and save locally
            $imgIn  = imagecreatefromstring($qr);
            $imgOut = imagecreate($width, $height);
            imagecopy($imgOut, $imgIn, 0, 0, 0, 0, $width, $height);
            imagepng($imgOut, $filename, 9, PNG_ALL_FILTERS);
            imagedestroy($imgIn);
            imagedestroy($imgOut);
        }

        return (file_exists($filename));
    }
}