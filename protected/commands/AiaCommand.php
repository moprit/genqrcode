<?php

/**
 * Class AIA Event *
 */
class AiaCommand extends CConsoleCommand {

    /**
     * Auto generate Visitor and QR Code
     *
     * @param int $offset
     * @param int $limit
     */
    public function actionAutogen($offset = 1, $limit = 100) {
        for ($i = $offset, $n = $offset + $limit; $i < $n; $i++) {
            $model       = new Visitor();
            $model->name = sprintf('ID: %s', $i);

            if (!$model->save()) {
                echo CVarDumper::dumpAsString($model->getErrors());
            } else {
                echo 'Created ' . $model->name . "\n";
            }
            echo 'Generate the QR Code for ' . $model->name . ": ";
            echo $model->genQrCode();
            echo "\n";
        }
        echo "Done \n";
    }

    public function actionInitPhoto() {
        $fileListOfDirectory     = array();
        $pathTofileListDirectory = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'photo';

        if (!is_dir($pathTofileListDirectory)) {
            die(" Invalid Directory");
        }

        if (!is_readable($pathTofileListDirectory)) {
            die("You don't have permission to read Directory");
        }

        foreach (new DirectoryIterator ($pathTofileListDirectory) as $file) {
            if ($file->getBasename() !== '..' && $file->getBasename() !== '.') {
                list ($id, $name) = explode('-', $file->getBasename(), 2);
                rename($pathTofileListDirectory . DIRECTORY_SEPARATOR . $file->getBasename(), $pathTofileListDirectory . DIRECTORY_SEPARATOR . trim($id) . '.jpg');
                array_push($fileListOfDirectory, "[" . trim($id) . "]" . $file->getBasename());
            }
        }
        echo 'done';
    }
}