<?php

/**
 * Class VnaCommand *
 */
class VnaCommand extends CConsoleCommand {


	public function actionSwap($key, $name) {
		$des = VnaVisitor::model()->findAllByAttributes(array('membership_number' => $key));
		if (count($des) !== 1) {
			$this->log('Have over 1 key: ' . $key . ' OR not found (' . count($des) . ')');
			die();
		}


		$source = VnaVisitor::model()->findAllBySql("SELECT * FROM visitor_vna WHERE `name` LIKE '%" . $name . "%'");
		if (count($source) !== 1) {
			$this->log('Have over 1 person with name: ' . $name . ' OR not found (' . count($source) . ')');
			die();
		}

		$this->log('swap... ');
		$this->_swap($des[0], $source[0]);
		$this->log('Done');
	}

	/**
	 * @param VnaVisitor $des
	 * @param VnaVisitor $source
	 */
	public function _swap($des, $source) {
		$tmp         = $des->key;
		$des->key    = $source->key;
		$source->key = $tmp;

		if (!$des->save()) {
			CVarDumper::dump($des->getErrors());

			return;
		}

		if (!$source->save()) {
			CVarDumper::dump($source->getErrors());

			return;
		}
	}


	public function actionGrabASeat() {
		$limit = 400; //rand(1, 50);
		/**
		 * @var VnaVisitor[] $models
		 */
		$models = VnaVisitor::model()->findAllByAttributes(array(
			"date_visited" => NULL
		), array(
			"order" => 'RAND()',
			'limit' => $limit
		));
		if ($models === NULL) {
			echo CJSON::encode(array('result' => FALSE, 'message' => 'Database is empty.'));
			Yii::app()->end();
		}

		if (count($models)) {
			foreach ($models as $model) {
				$model->checkIn();
				$this->log('[' . $model->id . '] Grab: ' . $model->seat_number . ' of ' . $model->table_number);
				sleep(rand(1, 10));
			}
		}
	}

	/**
	 * Auto generate Visitor and QR Code
	 *
	 * @param int $offset
	 * @param int $limit
	 */
	public function actionAutogen($offset = 1, $limit = 100) {
		$rows = VnaVisitor::model()->lastVisit($limit)->findAll();
		if (count($rows)) {
			foreach ($rows as $row) {
				echo 'Generate the QR Code for ' . $row->name . ": ";
				echo $row->genQrCode();
				echo "\n";
			}
		}

		echo "Done \n";
	}

	/**
	 *
	 */
	public function actionReadExcel() {

		$file_path   = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'visitor_vna3.xls';
		$sheet_array = Yii::app()->yexcel->readActiveSheet($file_path);

		$this->log("Here we go!!!");
		foreach ($sheet_array as $row) {
			$model = VnaVisitor::model()->findByAttributes(array('membership_number' => $row['A']));
			if ($model !== NULL && !empty($row['B'])) {
				//
			} else {
				$model                    = new VnaVisitor();
				$model->name              = ucwords(strtolower($row['B']));
				$model->membership_number = $row['A'];
				$model->company           = $row['C'];
				$model->job_title         = $row['D'];
				$model->food_set          = ($row['E'] == 'Set1' ? '1' : ($row['E'] == 'Set2' ? '2' : ($row['E'] == 'Set3' ? '3' : '')));
				$model->key               = md5(uniqid() . ' - ' . $model->membership_number);
				$model->save();
			}

			$this->log($model->key . " | " . $model->membership_number . " | " . $model->name . " | " . $model->company . " | " . $model->job_title . " | " . $model->food_set);
		}

		$this->log('Done');
	}

	/**
	 * @param string  $msg
	 * @param integer $level
	 */
	function log($msg, $level = 1) {
		echo $msg . "\n";
	}
}